<?php

namespace App\DataFixtures;

use App\Entity\Announce;
use App\Entity\Breed;
use App\Entity\Dog;
use App\Entity\Picture;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $images = [
            "https://images.dog.ceo/breeds/dhole/n02115913_3229.jpg",
            "https://images.dog.ceo/breeds/affenpinscher/n02110627_11345.jpg",
            "https://images.dog.ceo/breeds/airedale/n02096051_9359.jpg",
            "https://images.dog.ceo/breeds/poodle-standard/n02113799_7130.jpg",
            "https://images.dog.ceo/breeds/appenzeller/n02107908_5654.jpg",
            "https://images.dog.ceo/breeds/bulldog-boston/n02096585_6028.jpg",
            "https://images.dog.ceo/breeds/affenpinscher/n02110627_12893.jpg",
            "https://images.dog.ceo/breeds/lhasa/n02098413_21040.jpg",
            "https://images.dog.ceo/breeds/bulldog-boston/n02096585_19.jpg",
            "https://images.dog.ceo/breeds/pembroke/n02113023_4082.jpg",
            "https://images.dog.ceo/breeds/beagle/n02088364_161.jpg",
            "https://images.dog.ceo/breeds/mastiff-bull/n02108422_4978.jpg",
            "https://images.dog.ceo/breeds/setter-gordon/n02101006_2918.jpg",
            "https://images.dog.ceo/breeds/cairn/n02096177_911.jpg",
            "https://images.dog.ceo/breeds/australian-shepherd/leroy.jpg",
            "https://images.dog.ceo/breeds/retriever-flatcoated/n02099267_957.jpg",
            "https://images.dog.ceo/breeds/redbone/n02090379_3943.jpg",
            "https://images.dog.ceo/breeds/chihuahua/n02085620_8578.jpg",
            "https://images.dog.ceo/breeds/pointer-german/n02100236_2854.jpg",
            "https://images.dog.ceo/breeds/spaniel-irish/n02102973_2813.jpg",
            "https://images.dog.ceo/breeds/greyhound-italian/n02091032_9115.jpg",
            "https://images.dog.ceo/breeds/pyrenees/n02111500_1474.jpg",
            "https://images.dog.ceo/breeds/deerhound-scottish/n02092002_198.jpg",
            "https://images.dog.ceo/breeds/coonhound/n02089078_3078.jpg",
            "https://images.dog.ceo/breeds/spaniel-welsh/n02102177_1472.jpg",
            "https://images.dog.ceo/breeds/retriever-golden/n02099601_9301.jpg",
            "https://images.dog.ceo/breeds/terrier-sealyham/n02095889_4074.jpg",
            "https://images.dog.ceo/breeds/dachshund/dog-495122_640.jpg",
            "https://images.dog.ceo/breeds/schnauzer-miniature/n02097047_2417.jpg",
            "https://images.dog.ceo/breeds/hound-plott/hhh-23456.jpeg"
        ];

        $breeds = [
            (new Breed())->setName('border'),
            (new Breed())->setName('labrador'),
            (new Breed())->setName('flatcoat'),
            (new Breed())->setName('golden'),
            (new Breed())->setName('berger suisse'),
        ];

        $dogs = [];

        $toto = new User();
        $toto->setPassword(
            $this->encoder->encodePassword(
                $toto,
                'toto'
            )
        );
        $toto->setUsername('toto');
        $toto->setRoles(['ROLE_ADMIN']);
        $manager->persist($toto);

        $tata = new User();
        $tata->setPassword(
            $this->encoder->encodePassword(
                $tata,
                'tata'
            )
        );
        $tata->setUsername('tata');
        $tata->setRoles(['ROLE_ZOUBI']);
        $manager->persist($tata);



        for ($i = 0; $i < 40; $i++) {
            $dog = new Dog();
            $dog->setName("Chien " . $i);
            $dog->setBirthDate(new \DateTime());
            $dog->setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor consequat mauris, vel egestas augue vulputate id. In elementum ex eget feugiat tempor. Morbi vel risus mattis, feugiat purus in, sodales magna. Pellentesque a pretium elit. Nullam condimentum ipsum ac ante interdum porttitor. Nunc congue nulla velit. Donec hendrerit cursus urna, at mollis felis laoreet eu. In non nunc vitae mi laoreet congue. Praesent laoreet urna vitae neque vehicula aliquam. ");
            $dog->setIsAdopted(false);

            $rand = mt_rand(0, 1);
            for ($j = 0; $j <= $rand; $j++) {
                $dog->addBreed($breeds[mt_rand(0, count($breeds) - 1)]);
            }

            $rand = mt_rand(0, 2);
            for ($k = 0; $k <= $rand; $k++) {
                $picture = new Picture();
                $picture->setDog($dog);
                $dog->addPicture($picture->setLink($images[mt_rand(0, count($images) - 1)]));
                $manager->persist($picture);
            }

            array_push($dogs, $dog);

            foreach ($breeds as $breed) {
                $manager->persist($breed);
            }
            $manager->persist($dog);
        }

        for ($i = 1; $i < 11; $i++) {
            $announce = new Announce();
            $announce->setTitle('Annonce n°' . $i);
            $announce->setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor consequat mauris, vel egestas augue vulputate id. In elementum ex eget feugiat tempor. Morbi vel risus mattis, feugiat purus in, sodales magna. Pellentesque a pretium elit. Nullam condimentum ipsum ac ante interdum porttitor. Nunc congue nulla velit. Donec hendrerit cursus urna, at mollis felis laoreet eu. In non nunc vitae mi laoreet congue. Praesent laoreet urna vitae neque vehicula aliquam.");
            $announce->setIsFeeded(false);

            for ($f = 0; $f < mt_rand(1,2); $f++) {
                shuffle($dogs);
                $announce->addDog(array_pop($dogs));
            }

            $manager->persist($announce);
        }
        $manager->flush();
    }
}
