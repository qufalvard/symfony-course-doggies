<?php

namespace App\DataFixtures;

use App\Entity\Announce;
use App\Entity\Breed;
use App\Entity\Dog;
use App\Entity\Picture;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BreedFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $breeds = [
            (new Breed())->setName('border'),
            (new Breed())->setName('labrador'),
            (new Breed())->setName('flatcoat'),
            (new Breed())->setName('golden'),
            (new Breed())->setName('berger suisse'),
        ];

        foreach ($breeds as $breed) {
            $manager->persist($breed);
        }

        $manager->flush();
    }
}
