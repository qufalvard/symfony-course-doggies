<?php

namespace App\EventSubscriber;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param BeforeEntityPersistedEvent|BeforeEntityUpdatedEvent $event
     */
    public
    function onBeforeEntityPersistedEvent($event)
    {
        $user = $event->getEntityInstance();

        if (!$user instanceof User) {
            return;
        }

        if (empty($user->getPlainPassword())) {
            return;
        }
        $newPwd = $this->encoder->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($newPwd);
    }

    /**
     * @return array
     */
    public
    static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['onBeforeEntityPersistedEvent'],
            BeforeEntityUpdatedEvent::class => ['onBeforeEntityPersistedEvent'],
        ];
    }
}
