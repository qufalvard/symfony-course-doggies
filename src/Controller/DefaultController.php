<?php

namespace App\Controller;

use App\Entity\Announce;
use App\Entity\Breed;
use App\Entity\Contact;
use App\Entity\Dog;
use App\Entity\Picture;
use App\Form\ContactType;
use App\Repository\AnnounceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DefaultController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="home")
     * @param AnnounceRepository $announceRepository
     * @return Response
     */
    public function index(AnnounceRepository $announceRepository): Response
    {
        return $this->render('default/index.html.twig', [
            'announces' => $announceRepository->findByUpdated(false, 4),
        ]);
    }

    /**
     * @Route("/contact/{announce}", name="contact-new")
     * @param Request $request
     * @param Announce|null $announce
     * @return Response
     */
    public function contact(Request $request, Announce $announce = null): Response
    {
        $contact = new Contact();
        $contact->setAnnounce($announce);

        $form = $this->createForm(ContactType::class, $contact, [
            'method' => 'POST',
            'action' => $this->generateUrl('contact-new'),
        ]);

        // On dit explicitement au formulaire de traiter ce que contient la requête (objet Request)
        $form->handleRequest($request);

        // On regarde si le formulaire a été soumis ET est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // On enregistre
            $this->em->persist($contact);
            $this->em->flush();

            $this->addFlash('success', 'Messages bien envoyé');
            // Une fois que le formulaire est validé, on redirige pour éviter que l'utilisateur
            return $this->redirectToRoute('contact-new');
        }

        return $this->render('default/contact-general.html.twig', [
            'form' => $form->createView(), // On crée un objet FormView, qui sert à l'affichage de notre formulaire
        ]);
    }

    /**
     * @Route("/conditions", name="conditions")
     */
    public function conditions(): Response
    {
        return $this->render('default/conditions-adoption.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
}
