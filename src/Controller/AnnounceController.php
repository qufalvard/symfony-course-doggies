<?php

namespace App\Controller;

use App\Entity\Announce;
use App\Entity\Dog;
use App\Form\AnnounceType;
use App\Repository\AnnounceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/announce")
 */
class AnnounceController extends AbstractController
{
    /**
     * @Route("/", name="announce_index", methods={"GET"})
     */
    public function index(AnnounceRepository $announceRepository): Response
    {

        return $this->render('announce/index.html.twig', [
            'announces' => $announceRepository->findByUpdated(false, 0),
        ]);
    }

    /**
     * @Route("/new", name="announce_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $announce = new Announce();
        $form = $this->createForm(AnnounceType::class, $announce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($announce);
            $entityManager->flush();

            return $this->redirectToRoute('announce_index');
        }

        return $this->render('announce/new.html.twig', [
            'announce' => $announce,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="announce_show", methods={"GET"})
     */
    public function show(Announce $announce, EntityManagerInterface $entityManager): Response
    {
        return $this->render('announce/show.html.twig', [
            'announce' => $announce,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="announce_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Announce $announce): Response
    {
        $form = $this->createForm(AnnounceType::class, $announce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('announce_index');
        }

        return $this->render('announce/edit.html.twig', [
            'announce' => $announce,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="announce_delete", methods={"POST"})
     */
    public function delete(Request $request, Announce $announce): Response
    {
        if ($this->isCsrfTokenValid('delete' . $announce->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($announce);
            $entityManager->flush();
        }

        return $this->redirectToRoute('announce_index');
    }

    /**
     * @Route("/adopt/{dog}/{announce}", name="adopt")
     */
    public function adopt(Dog $dog, EntityManagerInterface $entityManager, Announce $announce)
    {
        $dog->setIsAdopted(true);
        $entityManager->persist($dog);

        $nbDogs = count($announce->getDogs());
        $nbDogAdopted = 0;
        foreach ($announce->getDogs() as $dog) {
            if ($dog->getIsAdopted() == true) {
                $nbDogAdopted++;
                $announce->setUpdated(new \DateTime());
                $entityManager->persist($announce);
            }
        }

        if ($nbDogAdopted == $nbDogs) {
            $announce->setIsFeeded(true);
            $entityManager->persist($announce);
        }
        $entityManager->flush();
        return $this->render('announce/show.html.twig', [
            'announce' => $announce,
        ]);
    }
}
