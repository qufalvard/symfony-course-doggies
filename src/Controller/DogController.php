<?php

namespace App\Controller;

use App\Entity\Dog;
use App\Entity\User;
use App\Form\DogType;
use App\Repository\DogRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @IsGranted("ROLE_ZOUBI")
 * @Route("/dog")
 */
class DogController extends AbstractController
{
    /**
     * @Route("/", name="dog_index", methods={"GET"})
     * @param DogRepository $dogRepository
     * @return Response
     */
    public function index(DogRepository $dogRepository): Response
    {
        return $this->render('dog/index.html.twig', [
            'dogs' => $dogRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="new-dog", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $dog = new Dog();
        $form = $this->createForm(DogType::class, $dog);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $dog->setUser($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($dog);
            $entityManager->flush();

            return $this->redirectToRoute('dog_index');
        }

        return $this->render('dog/new.html.twig', [
            'dog' => $dog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dog_show", methods={"GET"})
     * @param Dog $dog
     * @return Response
     */
    public function show(Dog $dog): Response
    {
        return $this->render('dog/show.html.twig', [
            'dog' => $dog,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="dog_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Dog $dog
     * @return Response
     */
    public function edit(Request $request, Dog $dog): Response
    {
        $form = $this->createForm(DogType::class, $dog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dog_index');
        }

        return $this->render('dog/edit.html.twig', [
            'dog' => $dog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dog_delete", methods={"POST"})
     * @param Request $request
     * @param Dog $dog
     * @return Response
     */
    public function delete(Request $request, Dog $dog): Response
    {
        if ($this->isCsrfTokenValid('delete'.$dog->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($dog);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dog_index');
    }
}
