# Exercices Symfony cda17

Nous allons créer un site d'adoption de chiens. Pour cela, créer un projet Symfony local et faites un premier push sur un projet Gitlab / Github.

## Créer des pages (Controller + routes)

[Page du cours sur le routing](https://formation-hb.drakolab.fr/symfony/05-routing.html)

[Page du cours sur les controller](https://formation-hb.drakolab.fr/symfony/06-controllers.html)

- [ ] Créer une branche

Nous allons commencer par créer des pages (des routes, controllers et vues (basiques dans un premier temps)) :
- [ ] Une page d'accueil (Uri/chemin : `/`) pour expliquer le but de l'association / du site
- [ ] Une page listant les annonces (Uri/chemin : `/annonces`) avec des photos de chiens, leur nom et un descriptif
- [ ] Une page de contact (Uri/chemin : `/contact`) avec un formulaire de contact
- [ ] Une page de conditions d'adoptions (Uri/chemin : `/conditions`)

/!\ Je vous conseille de regrouper les pages dans les controllers (avoir plusieurs actions dans un même controller). Par exemple, la page d'accueil et les conditions d'adoption peuvent être dans un même controller `PageController` (ceci est un exemple ;) ).

Dans un premier temps, nous allons juste créer le(s) controller(s), les routes et des vues vides (avec éventuellement un Lorem Ipsum ou équivalent, pour tester)

- [ ] Vérifier que les pages répondent bien
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Créer un menu (Twig + routes)

[Page du cours sur le routing](https://formation-hb.drakolab.fr/symfony/05-routing.html)

[Page du cours sur Twig](https://formation-hb.drakolab.fr/symfony/07-twig.html)

Maintenant que nous avons des pages, nous voulons pouvoir y accéder

- [ ] Créer une branche

- [ ] Créer un menu (basique) avec les liens des 4 pages
  - [ ] `/`
  - [ ] `/annonces`
  - [ ] `/contact`
  - [ ] `/conditions`
  
Pour se faire, utiliser la fonction `path()` de Twig ([documentation](https://symfony.com/doc/current/reference/twig_reference.html#path))

/!\ Si votre vue hérite d'une autre et modifie l'un de ces blocs, le contenu du parent est écrasé.
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Intégrer Bootstrap (Twig)

- [ ] Créer une branche

- [ ] [Télécharger Bootstrap](https://github.com/twbs/bootstrap/releases/download/v5.0.0-beta3/bootstrap-5.0.0-beta3-dist.zip) pour l'avoir dans les dossiers `public/css` et `public/js`
- [ ] Utiliser [la fonction `asset()`](https://symfony.com/doc/current/reference/twig_reference.html#asset) pour charger le css et le js de Bootstrap
- [ ] Améliorer le rendu votre menu à l'aide de Bootstrap (composant `navbar` par exemple)
  - [ ] Appliquer la classe `active` sur la page en cours 
    - [ ] Utiliser la valeur contenue dans `app.request.get('_route')` pour récupérer la route actuelle
    - [ ] Utiliser [une condition](https://twig.symfony.com/doc/3.x/tags/if.html) pour comparer avec la route de votre lien
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Ajouter du contenu (Twig)

Maintenant que nous avons des pages et des liens entre elles, donnons un peu de contenu à ces différentes pages.

- [ ] Créer une branche

- [ ] Ajouter un formulaire de contact HTML dans la page `/contact`
- [ ] Un texte aléatoire dans les pages `/conditions` et `/`
- [ ] Une liste de `card` bootstrap dans la page `/annonces`
  - [ ] vous pouvez ajouter des données dans un tableau PHP dans le controller et le passer en paramètre à la vue (utiliser alors [la boucle for](https://twig.symfony.com/doc/3.x/tags/for.html) pour afficher les différents éléments dans la vue Twig)
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Créer des entités (Doctrine)

[Page du cours sur Doctrine](https://formation-hb.drakolab.fr/symfony/09-doctrine.html)

Maintenant, concevons la Base de Données (BdD) de notre futur site.

- [ ] Créer une branche

- [ ] Créer un MCD (basique) avec les différentes tables pour :
  - [ ] Enregistrer le contenu du formulaire de contact
    - [ ] Un sujet
    - [ ] Un email
    - [ ] Une annonce (peut être vide)
    - [ ] Un message
  - [ ] Enregistrer / afficher une annonce, contenant :
    - [ ] Le titre de l'annonce
    - [ ] Une description
    - [ ] Le nom du ou des chiens
    - [ ] Leurs dates de naissance (Date) (cette date peut être vide, on ne connait pas toujours la date de naissance d'un chien ;) )
    - [ ] Leurs races
    - [ ] Des photos des chiens présentés dans l'annonce
    - [ ] Une description du ou des chiens
- [ ] Définir les entités à créer et les ajouter au projet (avec la commande `php bin/console make:entity`)
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Insérer des annonces

Si besoin d'images de chien, vous pourrez en trouver ici : [https://dog.ceo/dog-api/documentation/random](https://dog.ceo/dog-api/documentation/random)

- [ ] Créer une branche

Dans un premier temps, soit :

- [ ] Intégrer le [DoctrineFixtureBundle](https://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html) pour créer ce jeu de données (recommandé)
- [ ] Créer une action créant des annonces (et toutes les données associées)
- [ ] Les créer directement en base avec PhpMyAdmin
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Afficher des annonces et une annonce

- [ ] Créer une branche

- [ ] Afficher toutes les annonces dans la page `/annonces`
- [ ] Créer une nouvelle action avec une route `/annonce/{id}` où `id` est un identifiant d'une annonce
  - [ ] Afficher le détail d'une annonce (le titre, la description, les informations sur le(s) chien(s) dans une card par chien, etc.)
  - [ ] Utiliser le ParamConverter pour récupérer directement une annonce en paramètre de votre action (objet `Annonce` par exemple)
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes
  
## Gestion du formulaire de contact

[Le cours sur les formulaires](https://formation-hb.drakolab.fr/symfony/10-formulaires.html)

- [ ] Créer une branche

- [ ] Créer un formulaire à l'aide de la commande `php bin/console make:form` pour sauvegarder en base le contenu du formulaire de contact
- [ ] Afficher le formulaire dans la page `/contact` et le mettre en forme avec Bootstrap (si vous utilisez Bootstrap 4, un thème est déjà prêt, sinon, vous devrez ajouter les classes / créer un thème)
- [ ] En gérer la soumission pour que les données soient enregistrées en base

### Validation du formulaire

[La section du cours sur la validation](https://formation-hb.drakolab.fr/symfony/10-formulaires.html#validation)

- [ ] Valider les données et afficher les messages d'erreur nécessaire
    - [ ] Le sujet ne doit pas être vide
    - [ ] L'email ne doit pas être vide et doit être un email valide
    - [ ] L'annonce peut être vide
    - [ ] Le message ne doit pas être vide et doit faire entre 50 et 2500 caractères
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Améliorations du modèle

- [ ] Créer une branche

Faire en sorte que vos entités respectent les contraintes suivantes :
- [ ] Un chien peut avoir plusieurs races (s'il est croisé)
- [ ] Une annonce peut être liée à : 
  - [ ] plusieurs contacts
  - [ ] plusieurs chiens (un chien est lié à une seule annonce maximum)
- [ ] Un chien peut être adopté.
- [ ] Une annonce peut être pourvue (quelqu'un a adopté les chiens de l'annonce).
- [ ] Une annonce a une date de création (`created_at`) et une date de dernière mise à jour (`updated_at`) (vous pouvez utiliser [les extensions Doctrine](https://github.com/stof/StofDoctrineExtensionsBundle))
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

### Changements dans le fonctionnel 

- [ ] Créer une branche

- [ ] Si un chien est adopté :
  - [ ] s'il n'est pas seul dans une annonce, l'afficher sur l'annonce
  - [ ] s'il est seul dans une annonce, l'annonce est pourvue et peut donc être fermée
- [ ] Une annonce pourvue n'apparait plus dans la liste
  
/!\ Pour ne plus afficher une annonce, vous pouvez ajouter une propriété (`hidden`, un booléen, par exemple) dans votre entité d'annonce, et créer une méthode dans votre repository pour ne récupérer que les annonces "visibles" (dans mon exemple, avec la propriété `hidden`à `false`).
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Page d'accueil

- [ ] Créer une branche

Sur la page d'accueil, 
- [ ] Afficher les 4 annonces mises à jour le plus récemment (et qui ne sont pas pourvues)
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes
  
## Contacter quelqu'un pour un chien

Notre formulaire de contact peut être lié à une annonce nous allons maintenant lier les deux

- [ ] Créer une branche

- [ ] Dans une page d'annonce, ajouter un lien vers la page `/contact`, avec un paramètre supplémentaire : l'identifiant de l'annonce (utiliser le second paramètre de `path()` pour transmettre ce paramètre)
- [ ] Dans l'action de la page de contact, récupérer ce paramètre et, s'il n'est pas `null`, l'ajouter à l'objet contact. 
  - [ ] Le champ indiquant l'annonce doit alors être rempli avec la bonne annonce
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes
  
## Interface d'administration

- [ ] Créer une branche
  
- [ ] Utiliser [Easy Admin Bundle](https://symfony.com/doc/current/bundles/EasyAdminBundle/index.html) pour créer une interface d'administration pour vos différentes entités
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Connexion et droits

- [ ] Créer une branche

- [ ] Créer une entité `User` (avec la commande `make:user`, certaines propriétés sont spéciales)
- [ ] Créer un système d'authentification avec `make:auth` et personnaliser la page de connexion
- [ ] Définir les rôles disponibles (dans le fichier `config/security.yaml`)
- [ ] Restreindre l'accès à l'administration aux utilisateurs ayant le rôle `ROLE_ADMIN`
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes

## Traductions

- [ ] Créer une branche

- [ ] Mettre en place des fichiers de traduction pour votre site
- [ ] Remplacer tous les textes de vos fichiers Twig et PHP par des traductions
  
- [ ] Créer un/des commits dans votre branche
- [ ] Créer une Merge/Pull Request
- [ ] Faites la relire par une ou deux personnes
